load('SubjectData.mat');
eL = 2;
Fs = 256;
cuts = {5:7, 1:4}; % Video and Audio channels
chNames = {'T3','Cz','T4','Oz','O1','Oz','O2'};
cha = [6 8 10 15 14 15 16];
fb = 2.5;

%% 
hmmData = struct();
for cat = {'sine','words'}
    for c = chNames(cuts{1})
        for f = {'07','10'}
            hmmData.(strcat(cat{1},c{1},f{1},'index')) = 0;
        end
    end
end
for cat = {'sine','words'}
    for c = chNames(cuts{2})
        for f = {'37','43'}
            hmmData.(strcat(cat{1},c{1},f{1},'index')) = 0;
        end
    end
end

%% 
for n = 1:length(subjData)
    toIndexData = subjData(n).toIndex;
    cat = subjData(n).SorW;
    for j = 1:size(toIndexData,2)
        
        if (toIndexData{4,j} == 10)
            for c = chNames(cuts{1})
                hmmData.(strcat(cat,c{1},'10','index')) = hmmData.(strcat(cat,c{1},'10','index')) + 1;
            end
        else
            for c = chNames(cuts{1})
                hmmData.(strcat(cat,c{1},'07','index')) = hmmData.(strcat(cat,c{1},'07','index')) + 1;
            end
        end
        
        if (toIndexData{5,j} == 37)
            for c = chNames(cuts{2})
                hmmData.(strcat(cat,c{1},'37','index')) = hmmData.(strcat(cat,c{1},'37','index')) + 1;
            end
        else
            for c = chNames(cuts{2})
                hmmData.(strcat(cat,c{1},'43','index')) = hmmData.(strcat(cat,c{1},'43','index')) + 1;
            end
        end
    end
end

%% 
for n = 1:length(subjData)
    disp(n);
    toIndexData = subjData(n).toIndex;
    cat = subjData(n).SorW;
    for j = 1:size(toIndexData,2)
        
        if (toIndexData{4,j} == 10)
            for c = chNames(cuts{1})
                hmmData.(strcat(cat,c{1},'10')) = cell(1,hmmData.(strcat(cat,c{1},'10','index')));
                hmmData.(strcat(cat,c{1},'10','index')) = 1;
            end
        else
            for c = chNames(cuts{1})
                hmmData.(strcat(cat,c{1},'07')) = cell(1,hmmData.(strcat(cat,c{1},'07','index')));
                hmmData.(strcat(cat,c{1},'07','index')) = 1;
            end
        end
        
        if (toIndexData{5,j} == 37)
            for c = chNames(cuts{2})
                hmmData.(strcat(cat,c{1},'37')) = cell(1,hmmData.(strcat(cat,c{1},'37','index')));
                hmmData.(strcat(cat,c{1},'37','index')) = 1;
            end
        else
            for c = chNames(cuts{2})
                hmmData.(strcat(cat,c{1},'43')) = cell(1,hmmData.(strcat(cat,c{1},'43','index')));
                hmmData.(strcat(cat,c{1},'43','index')) = 1;
            end
        end
    end
end

%% 

for n = 1:length(subjData)
    disp(n);
    HDR = sopen(strcat('C:\Users\USER\Desktop\Everything related to Major\MAJOR PROJECT\DATA\ProperData\', ... 
        subjData(n).FName,'.edf'), 'r');
    [S,HDR] = sread(HDR);
    HDR = sclose(HDR);
    
    toIndexData = subjData(n).toIndex;
    fir = cell2mat(toIndexData(1,:));
    extra = cell2mat(toIndexData(2,:));
    cat = subjData(n).SorW;
    for j = 1:size(toIndexData,2)
        clear dat;
        tosegdat = S(fir(j)*Fs+1+extra(j):(fir(j)+24)*Fs+extra(j),cha(cuts{1}));
        chan = chNames(cuts{1});
        
        for i = 1:size(tosegdat,2)
            seg = buffer(tosegdat(:,i),Fs*eL,ceil(50/100*Fs*eL));
            dat(:,:,i) = seg(:,2:end);
        end
        
        for i = 1:size(dat,3)
            
            for segm = 1:size(dat,2)
                
                cw07(segm) = mean(abs(cwt(dat(:,segm,i),Fs,strcat('cmor',num2str(fb),'-7.5'))).^2) + ...
                    mean(abs(cwt(dat(:,segm,i),Fs,strcat('cmor',num2str(fb),'-15'))).^2);
                cw10(segm) = mean(abs(cwt(dat(:,segm,i),Fs,strcat('cmor',num2str(fb),'-10'))).^2) + ...
                    mean(abs(cwt(dat(:,segm,i),Fs,strcat('cmor',num2str(fb),'-20'))).^2);
            end
            
            if (toIndexData{4,j} == 7.5)
                c = hmmData.(strcat(cat,chan{i},'07'));
                c{hmmData.(strcat(cat,chan{i},'07','index'))} = cast(cw07 > cw10,'uint8') + 1;
                hmmData.(strcat(cat,chan{i},'07','index')) = hmmData.(strcat(cat,chan{i},'07','index')) + 1;
                hmmData.(strcat(cat,chan{i},'07')) = c;
            else
                c = hmmData.(strcat(cat,chan{i},'10'));
                c{hmmData.(strcat(cat,chan{i},'10','index'))} = cast(cw07 < cw10,'uint8') + 1;
                hmmData.(strcat(cat,chan{i},'10','index')) = hmmData.(strcat(cat,chan{i},'10','index')) + 1;
                hmmData.(strcat(cat,chan{i},'10')) = c;
            end
            
        end
        
        tosegdat = S(fir(j)*Fs+1+extra(j):(fir(j)+24)*Fs+extra(j),cha(cuts{2}));
        chan = chNames(cuts{2});
        clear dat;
        for i = 1:size(tosegdat,2)
            seg = buffer(tosegdat(:,i),Fs*eL,ceil(50/100*Fs*eL));
            dat(:,:,i) = seg(:,2:end);
        end
        
        for i = 1:size(dat,3)
            
            for segm = 1:size(dat,2)
                
                cw37(segm) = mean(abs(cwt(dat(:,segm,i),Fs,strcat('cmor',num2str(fb),'-37'))).^2);
                cw43(segm) = mean(abs(cwt(dat(:,segm,i),Fs,strcat('cmor',num2str(fb),'-43'))).^2);
                
            end
            
            if (toIndexData{5,j} == 37)
                c = hmmData.(strcat(cat,chan{i},'37'));
                c{hmmData.(strcat(cat,chan{i},'37','index'))} = cast(cw37 > cw43,'uint8') + 1;
                hmmData.(strcat(cat,chan{i},'37','index')) = hmmData.(strcat(cat,chan{i},'37','index')) + 1;
                hmmData.(strcat(cat,chan{i},'37')) = c;
            else
                c = hmmData.(strcat(cat,chan{i},'43'));
                c{hmmData.(strcat(cat,chan{i},'43','index'))} = cast(cw37 < cw43,'uint8') + 1;
                hmmData.(strcat(cat,chan{i},'43','index')) = hmmData.(strcat(cat,chan{i},'43','index')) + 1;
                hmmData.(strcat(cat,chan{i},'43')) = c;
            end
            
        end
        
    end
end