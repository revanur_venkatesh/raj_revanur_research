clear all;
load('HMM_sft.mat');
clc;
fields = fieldnames(hmmData);
final_fields = cell(0);
i =0
f = char(fields')
s = size(f,1)
for i=1:1:s
    disp(f(i,:))
    strfind(f(i,:), 'index')
    if(strfind(f(i,:), 'index')==[])
        disp(f(i,:))
        final_fields{i} = f(i,:);
    end
end
disp(final_fields);
hmmDataDerived = hmmData;
for times = 5:5:25
    for i = 1:1:size(final_fields,2)
        field = final_fields(:,i)
        field = strtrim(field)
        v = hmmDataDerived.(strtrim(field))
        for index = 1:size(v, 2)
            a = v(1,index);
            a{1} = a{1}(1:times-1);
            v(1,index) = a;
        end
        hmmDataDerived.(field) = v;
    end
    fname = ['HMM_sft_', num2str(times), 'sec'];
    save(fname, 'hmmDataDerived')
end
clear hmmDataDerived;
for times= 5:5:25
    fname = ['HMM_sft_', num2str(times), 'sec.mat'];
    load(fname);
    for i = 1:1:size(final_fields,2)
        field = final_fields(:,i);
         field = strtrim(field);
        v = hmmData.(field);
        for index = 1:size(v, 2)
            hmmDataDerived.(field){index} = hmmDataDerived.(field){index}(1:times-1)
        end
    end
end