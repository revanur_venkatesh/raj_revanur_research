load('X.mat')
load('Y.mat')
load('Loglikelihoods_HMM_CCA.mat')
X = loglikelihoods_quad_dimension;
% X(:,2:3) = -X(:,2:3);
B = (X' * X)\X' * Y;


min_vals = floor(min(log_likelihoods, [], 2));
min_x = min_vals(1);
min_y = min_vals(2);

max_vals = ceil(max(log_likelihoods, [], 2));
max_x = max_vals(1);
max_y = max_vals(2);

data = [];
for x = min_x:0.1:max_x
    for y = min_y:0.1:max_y
        Y_hat = [1 x y x^2 y^2 x*y] * B;
        [~, I] = max(Y_hat);
        data = [data [x;y;I]]; 
    end
end


figure;
plot(data(1,data(end,:)==1), data(2,data(end,:)==1), 'o');
hold on;
plot(data(1,data(end,:)==2), data(2,data(end,:)==2), 'x');


diff = B(:,1) - B(:,2);

a = diff(4);
b = diff(5);
h = diff(6) / 2;
g = diff(2) / 2;
f = diff(3) / 2;
c = diff(1);

A = [
    a h;
    h b
];
delta = [
    a h g;
    h b f;
    g f c
];


alpha_beta = A\[-g; -f];

