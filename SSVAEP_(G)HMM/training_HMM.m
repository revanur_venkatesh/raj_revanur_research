function [prior_learnt, transmat_learnt, emmat_learnt] = training_HMM(train_data, class)
%training_HMM Summary of this function goes here
%   Detailed explanation goes here

% O = 2; % number of distinct observations
% Q = 2; % number of internal states
max_iter = 20; % number of iterations for EM
% T = number of observations in one observation sequence 
% nex = number of samples or sessions or observation sequences

% Estimates for prior, transition and emission matrices
if class == 1
    prior_estimate = [1 0];
    transmat_estimate = [
        0.7 0.3;
        0.7 0.3
    ];
else
    prior_estimate = [0 1];
    transmat_estimate = [
        0.3 0.7;
        0.3 0.7
    ];
end
emmat_estimate = [
    0.95 0.05;
    0.05 0.95
];

% improve guess of parameters using EM
[LL, prior_learnt, transmat_learnt, emmat_learnt] = learn_dhmm(train_data, prior_estimate, transmat_estimate, emmat_estimate, max_iter);

% TODO plot LL - get training learning curve
end

