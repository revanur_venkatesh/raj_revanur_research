clc;
clear all;
tic;
load('HMM_cca.mat');

%% For Video
v_types = {'sine','words'};
v_electrodes = {'O1','Oz','O2'};
v_freq = {'07','10'};
array_trans = [];
array_emmis = [];
store_index_test= {};
count = 0;
jump_index = 0;
t = 1;

  hmmdata_for7hz = hmmData.sine07;
  

  
  labels_for7hz = ones(1,(size(hmmData.sine07,2)));
  for j=1:(size(hmmData.sine10,2))
      hmmdata_for7hz(size(hmmData.sine07,2)+j) = hmmData.sine10(j);
      labels_for7hz(1,size(hmmData.sine07,2)+j) = 2;
  end
  indexvector=randperm(size(hmmdata_for7hz,2));
  for k=1:size(hmmdata_for7hz,2)
      rearranged_cellhmmdata_for7hz(k) = hmmdata_for7hz(indexvector(k));
      rearranged_celllabelsdata_for7hz(k) = labels_for7hz(indexvector(k));
  end
  [train, test] = crossvalind('HoldOut', size(rearranged_cellhmmdata_for7hz,2), 0.1);
  index_train_data = (train)'.*(1:size(rearranged_cellhmmdata_for7hz,2));
  disp(size(rearranged_cellhmmdata_for7hz,2));
  index_test_data = (test)'.*(1:size(rearranged_cellhmmdata_for7hz,2));
  index_train_data(index_train_data==0)=[];
  index_test_data(index_test_data==0)=[];
  count = count + 1;
  index_test_glob{count} = index_test_data;
  temp_cell = rearranged_cellhmmdata_for7hz;
  temp_cell_arrays_train_data = temp_cell(index_train_data);
  temp_cell_arrays_test_data = temp_cell(index_test_data); % test data
  temp_cell_train_label = rearranged_celllabelsdata_for7hz(index_train_data);
  temp_cell_test_label = rearranged_celllabelsdata_for7hz(index_test_data);% test labels with no order
  
  for i=1:size(temp_cell_train_label,2)
      for j=i:size(temp_cell_train_label,2)
          if (temp_cell_train_label(i) > temp_cell_train_label(j))
              var_temp_label = temp_cell_train_label(i);
              var_temp_data = temp_cell_arrays_train_data(i);
              temp_cell_train_label(i) = temp_cell_train_label(j);
              temp_cell_arrays_train_data(i) = temp_cell_arrays_train_data(j);
              temp_cell_train_label(j) = var_temp_label;
              temp_cell_arrays_train_data(j) = var_temp_data;
          end
      end
  end
  
  combined_dataset_for_modelling_first = {};
  combined_dataset_for_modelling_second = {};
  count = 1;
  count1 = 1;
  for i=1:size(temp_cell_train_label,2)
    if(temp_cell_train_label(i) == 1)
        combined_dataset_for_modelling_first(count) = temp_cell_arrays_train_data(i);
        count = count + 1;
    end
    if(temp_cell_train_label(i) == 2)
        combined_dataset_for_modelling_second(count1) = temp_cell_arrays_train_data(i);
        count1 = count1 + 1;
    end
  end
  combined_dataset_for_modelling_first
  combined_dataset_for_modelling_second
  
  pause;
  i =1
  [real_trans_first real_emiss_first] = hmmtrain(combined_dataset_for_modelling_first,[0.7 0.3;0.3 0.7],[0.95 0.05;0.05 0.95])
  cell_real_trans_and_emiss_for_first_video {t,i} = [strcat(v_types(1),v_freq(1)) real_trans_first real_emiss_first]
  u_train(t,i) = log_lik_dhmm(combined_dataset_for_modelling_first,[0.5 0.5],real_trans_first,real_emiss_first);
  
  i =2
  [real_trans_second real_emiss_second] = hmmtrain(combined_dataset_for_modelling_second,[0.7 0.3;0.3 0.7],[0.95 0.05;0.05 0.95]);
  cell_real_trans_and_emiss_for_first_video {t,i} = [strcat(v_types(1),v_freq(1)) real_trans_second real_emiss_second];
  u_train(t,i) = log_lik_dhmm(combined_dataset_for_modelling_first,[0.5 0.5],real_trans_second,real_emiss_second);
 
  
  for i=1:size(temp_cell_test_label,2)
      for j=i:size(temp_cell_test_label,2)
          if (temp_cell_test_label(i) > temp_cell_test_label(j))
              var_temp_label = temp_cell_test_label(i);
              var_temp_data = temp_cell_arrays_test_data(i);
              temp_cell_test_label(i) = temp_cell_test_label(j);
              temp_cell_arrays_test_data(i) = temp_cell_arrays_test_data(j);
              temp_cell_test_label(j) = var_temp_label;
              temp_cell_arrays_test_data(j) = var_temp_data;
          end
      end
  end
  
  for i=1:size(temp_cell_test_label,2)
    u_test(i,1) = log_lik_dhmm(temp_cell_arrays_test_data(i),[0.5 0.5],cell_real_trans_and_emiss_for_first_video{1,1}{1,2},cell_real_trans_and_emiss_for_first_video{1,1}{1,3});
    u_test(i,2) = log_lik_dhmm(temp_cell_arrays_test_data(i),[0.5 0.5],cell_real_trans_and_emiss_for_first_video{1,2}{1,2},cell_real_trans_and_emiss_for_first_video{1,2}{1,3});   
  end
  
toc;