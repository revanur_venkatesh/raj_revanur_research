function [accuracy, log_likelihood] = testing_HMM(prior_learnt_1, transmat_learnt_1, emmat_learnt_1, prior_learnt_2, transmat_learnt_2, emmat_learnt_2, test_data, test_labels)
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here
% test_data = sequence * number_of_samples

log_likelihood = zeros(2, size(test_data, 2));
% disp('print model 1: ');disp( model_1);
% prior_learnt_1 = model_1(1);
% transmat_learnt_1 = model_1(2); 
% emmat_learnt_1 = model_1(3);
% prior_learnt_2 = model_2(1);
% transmat_learnt_2 = model_2(2); 
% emmat_learnt_2 = model_2(3);

for index = 1:size(test_data,2)
    % solve against Model 1
    log_likelihood(1, index) = log_lik_dhmm(test_data(:,index), prior_learnt_1, transmat_learnt_1, emmat_learnt_1);
    % solve against Model 2
    log_likelihood(2, index) = log_lik_dhmm(test_data(:,index), prior_learnt_2, transmat_learnt_2, emmat_learnt_2);
end

test_results = (log_likelihood(1, :) < log_likelihood(2, :)) + 1;
equality = test_results == test_labels;
accuracy = sum(equality) * 100 / length(equality);

end

