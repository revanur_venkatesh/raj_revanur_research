clc;
tic;
wors = 2;
%load('webdatahmm.mat');
load('HMM_cca.mat');
%%
F1=fopen('cca_prob_data.txt','a');
%% For Video
v_types = {'sine','words'};
v_electrodes = {'O1','Oz','O2'};
v_freq = {'07','10'};
array_trans = [];
array_emmis = [];
store_index_test= {};
count = 0;
jump_index = 0;

t = 1;
%for i=1:size(v_electrodes,2)
    for j=1:size(v_freq,2)
        
        access_data_string = strcat(v_types(wors),v_freq(j));
        access_size_string = strcat(v_types(wors),v_freq(j),'index');
        %         for k=1:hmmData.(access_size_string{1,1})-1
        %
        %             array_temp_hidden = hmmData.(access_data_string{1,1}){1,k};
        %             array_temp_visible = ones(size(array_temp_hidden));
        %             [trans emiss] = hmmestimate(array_temp_visible,array_temp_hidden);
        %             array_trans = [array_trans trans];
        %             array_emmis = [array_emmis emiss];
        %
        %         end
        
        [train, test] = crossvalind('HoldOut', size(hmmData.(access_data_string{1,1}),2), 0.1);
        index_train_data = (train)'.*(1:size(hmmData.(access_data_string{1,1}),2));
        disp(size(hmmData.(access_data_string{1,1}),2));
        index_test_data = (test)'.*(1:size(hmmData.(access_data_string{1,1}),2));
        index_train_data(index_train_data==0)=[];
        index_test_data(index_test_data==0)=[];
        count = count + 1;
        index_test_glob{count} = index_test_data;
        
        %         store_index_test{i,j} = index_test_data;
        
        temp_cell = hmmData.(access_data_string{1,1});
        temp_cell_arrays_train = temp_cell(index_train_data);
        temp_cell_arrays_test = temp_cell(index_test_data);
        
        [real_trans real_emiss] = hmmtrain(temp_cell_arrays_train,[0.7 0.3;0.3 0.7],[0.95 0.05;0.05 0.95]);
        cell_real_trans_and_emiss_video {t,j} = [access_data_string real_trans real_emiss];
        u_train(t,j) = log_lik_dhmm(temp_cell_arrays_train,[0.5 0.5],real_trans,real_emiss);
    end
%end


 %i = 1;
    %for j=1:size(v_electrodes,2)
        
        access_data_string_07 = strcat(v_types(wors),'07');
        %access_size_string = strcat(v_electrodes(i),v_freq(j),'index');
        access_data_string_10 = strcat(v_types(wors),'10');
        
        % FOR TESTING DATA 07 ------------------------------------------------
        [train, test] = crossvalind('HoldOut', size(hmmData.(access_data_string_07{1,1}),2), 0.1);
        index_test_data = (test)'.*(1:size(hmmData.(access_data_string_07{1,1}),2));
        
        index_test_data(index_test_data==0)=[];
        temp_cell = hmmData.(access_data_string_07{1,1});
        jump_index = jump_index + 1;
        array_test_obtained = index_test_glob(jump_index);
        test_data_07 = temp_cell(array_test_obtained{1,1});
        
        u_test(1) = log_lik_dhmm(test_data_07,[0.5 0.5],cell_real_trans_and_emiss_video{1}{1,2},cell_real_trans_and_emiss_video{1}{1,3});
        u_test(2) = log_lik_dhmm(test_data_07,[0.5 0.5],cell_real_trans_and_emiss_video{2}{1,2},cell_real_trans_and_emiss_video{2}{1,3});
        fprintf(F1,'%s\t%s\n',u_test(i,1),u_test(i,2));
        % FOR TESTING DATA 10 -------------------------------------------------
        [train, test] = crossvalind('HoldOut', size(hmmData.(access_data_string_10{1,1}),2), 0.1);
        index_test_data = (test)'.*(1:size(hmmData.(access_data_string_10{1,1}),2));
        
        index_test_data(index_test_data==0)=[];
        temp_cell = hmmData.(access_data_string_10{1,1});
        jump_index = jump_index + 1;
        array_test_obtained = index_test_glob(jump_index);
        test_data_10 = temp_cell(array_test_obtained{1,1});
        u_test(3) = log_lik_dhmm(test_data_10,[0.5 0.5],cell_real_trans_and_emiss_video{1}{1,2},cell_real_trans_and_emiss_video{1}{1,3});
        u_test(4) = log_lik_dhmm(test_data_10,[0.5 0.5],cell_real_trans_and_emiss_video{2}{1,2},cell_real_trans_and_emiss_video{2}{1,3});
        fprintf(F1,'%s\t%s\n',u_test(i,3),u_test(i,4));
   % end


a1_s = u_test(1,1) > u_test(1,2);
b1_s = u_test(1,4) > u_test(1,3);
c1_s = sum(a1_s)+sum(b1_s);
efficiency_v_sineorwords = c1_s/2 *100;
f1 = fopen('Results.txt','w');
fprintf(f1,'%d\n',efficiency_v_sineorwords);
fclose(f1);
toc;
%%