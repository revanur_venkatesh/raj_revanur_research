function [] = GHMM_Audio_web (wors)
clc;

%% CORE PART

attach = {'KPMstats','netlab3.3','HMM','GraphViz','KMPtools'};
initial_path = 'C:/Users/Revanur Venkatesh/Downloads/bnt-master/bnt-master';

addpath 'C:\Users\Revanur Venkatesh\Documents\MATLAB\Final_demonstration\bnt-master\bnt-master\KPMstats';
addpath 'C:\Users\Revanur Venkatesh\Documents\MATLAB\Final_demonstration\bnt-master\bnt-master\netlab3.3';
addpath 'C:\Users\Revanur Venkatesh\Documents\MATLAB\Final_demonstration\bnt-master\bnt-master\HMM';
addpath 'C:\Users\Revanur Venkatesh\Documents\MATLAB\Final_demonstration\bnt-master\bnt-master\GraphViz';
addpath 'C:\Users\Revanur Venkatesh\Documents\MATLAB\Final_demonstration\bnt-master\bnt-master\KPMtools';


%% VandA Init
%load('webdatahmmgmm.mat');
load('GHMM_cwt.mat');
M = 2;
Q = 2;


%% FOR AUDIO

a_type = {'sine','words'};
a_electrodes = {'T3','Cz','T4','Oz'};
a_freq = {'37','43'};
array_trans = [];
array_emmis = [];
store_index_test= {};
count = 0;
jump_index = 0;

t = 1;
    for i=1:size(a_electrodes,2)
        for j=1:size(a_freq,2)
            
            access_data_string = strcat(a_type(wors),a_electrodes(i),a_freq(j));
            access_size_string = strcat(a_type(wors),a_electrodes(i),a_freq(j),'index');
            %             for k=1:hmmData.(access_size_string{1,1})-1
            %
            %                 array_temp_hidden = hmmData.(access_data_string{1,1}){1,k};
            %                 array_temp_visible = ones(size(array_temp_hidden));
            %                 [trans emiss] = hmmestimate(array_temp_visible,array_temp_hidden);
            %                 array_trans = [array_trans trans];
            %                 array_emmis = [array_emmis emiss];
            %
            %             end
            
            O = size(hmmData.(access_data_string{1,1}) ,1);
            T = size(hmmData.(access_data_string{1,1}),2);
            nex = size(hmmData.(access_data_string{1,1}),3);
            
            [train, test] = crossvalind('HoldOut', nex, 0.5);
            index_train_data = (train)'.*(1:nex);
            index_test_data = (test)'.*(1:nex);
            
            index_train_data(index_train_data==0)=[];
            index_test_data(index_test_data==0)=[];
            
            training_data_audio = hmmData.(access_data_string{1,1})(:,:,index_train_data)
            testing_data = hmmData.(access_data_string{1,1})(:,:,index_test_data);% cpy for test
            
            count = count + 1;
            index_test_glob{count} = index_test_data;
            
            prior0 = [0.5 0.5];
            transmat0 = [1 0;0 1];
            
            cov_type = 'full';
            [mu0, Sigma0] = mixgauss_init(Q*M, reshape(training_data_audio, [O T*(size(training_data_audio,3))]), cov_type);
            mu0 = reshape(mu0, [O Q M]);
            Sigma0 = reshape(Sigma0, [O O Q M]);
            mixmat0 = mk_stochastic(rand(Q,M));
            
            [ll_trace, prior, transmat, mu, sigma, mixmat] = mhmm_em(training_data_audio, prior0, transmat0, mu0, Sigma0, mixmat0, 'max_iter', 100);
            loglik_train{t,i,j} = mhmm_logprob(training_data_audio, prior, transmat, mu, sigma, mixmat);
            cell_real_trans_and_emiss_audio {t,i,j} = [access_data_string ll_trace, prior, transmat, mu, sigma, mixmat];
            
            %[real_trans real_emiss] = hmmtrain(hmmData.(access_data_string{1,1}),[0.7 0.3;0.3 0.7],[0.95 0.05;0.05 0.95]);
            %cell_real_trans_and_emiss_audio {t,i,j} = [access_data_string real_trans real_emiss];
            %u1(i,j) = log_lik_dhmm(hmmData.(access_data_string{1,1}),[1 0],real_trans,real_emiss);
            
        end
    end


i = 1;
    for j=1:size(a_electrodes,2)
        
        access_data_string_37 = strcat(a_type(wors),a_electrodes(j),'37');
        %        access_size_string = strcat(a_electrodes(i),a_freq(j),'index');
        access_data_string_43 = strcat(a_type(wors),a_electrodes(j),'43');
        
        % FOR TESTING DATA 37 -------------------------------------------------
        
        %temp_cell = hmmData.(access_data_string_07{1,1});
        jump_index = jump_index + 1;
        array_test_obtained = index_test_glob(jump_index);
        %test_data_07 = temp_cell(array_test_obtained{1,1});
        testing_data_37 = hmmData.(access_data_string_37{1,1})(:,:,array_test_obtained{1,1});
        
        loglik_test_data(i,j,1) = mhmm_logprob(testing_data_37, cell_real_trans_and_emiss_audio{i,j,1}{1,3}, cell_real_trans_and_emiss_audio{i,j,1}{1,4}, cell_real_trans_and_emiss_audio{i,j,1}{1,5}, cell_real_trans_and_emiss_audio{i,j,1}{1,6}, cell_real_trans_and_emiss_audio{i,j,1}{1,7});
        loglik_test_data(i,j,2) = mhmm_logprob(testing_data_37, cell_real_trans_and_emiss_audio{i,j,2}{1,3}, cell_real_trans_and_emiss_audio{i,j,2}{1,4}, cell_real_trans_and_emiss_audio{i,j,2}{1,5}, cell_real_trans_and_emiss_audio{i,j,2}{1,6}, cell_real_trans_and_emiss_audio{i,j,2}{1,7});
        %     u_test(i,1) = log_lik_dhmm(test_data_07,[0.5 0.5],cell_real_trans_and_emiss_video{i,1}{1,2},cell_real_trans_and_emiss_video{i,1}{1,3});
        %     u_test(i,2) = log_lik_dhmm(test_data_07,[0.5 0.5],cell_real_trans_and_emiss_video{i,2}{1,2},cell_real_trans_and_emiss_video{i,2}{1,3});
        
        % FOR TESTING DATA 43 -------------------------------------------------
        %[train, test] = crossvalind('HoldOut', size(hmmData.(access_data_string_10{1,1}),2), 0.1);
        %index_test_data = (test)'.*(1:size(hmmData.(access_data_string_10{1,1}),2));
        
        %temp_cell = hmmData.(access_data_string_10{1,1});
        jump_index = jump_index + 1;
        array_test_obtained = index_test_glob(jump_index);
        %test_data_07 = temp_cell(array_test_obtained{1,1});
        testing_data_43 = hmmData.(access_data_string_43{1,1})(:,:,array_test_obtained{1,1});
        
        loglik_test_data(i,j,3) = mhmm_logprob(testing_data_43, cell_real_trans_and_emiss_audio{i,j,1}{1,3}, cell_real_trans_and_emiss_audio{i,j,1}{1,4}, cell_real_trans_and_emiss_audio{i,j,1}{1,5}, cell_real_trans_and_emiss_audio{i,j,1}{1,6}, cell_real_trans_and_emiss_audio{i,j,1}{1,7});
        loglik_test_data(i,j,4) = mhmm_logprob(testing_data_43, cell_real_trans_and_emiss_audio{i,j,2}{1,3}, cell_real_trans_and_emiss_audio{i,j,2}{1,4}, cell_real_trans_and_emiss_audio{i,j,2}{1,5}, cell_real_trans_and_emiss_audio{i,j,2}{1,6}, cell_real_trans_and_emiss_audio{i,j,2}{1,7});
        %     u_test(i,1) = log_lik_dhmm(test_data_07,[0.5 0.5],cell_real_trans_and_emiss_video{i,1}{1,2},cell_real_trans_and_emiss_video{i,1}{1,3});
        %     u_test(i,2) = log_lik_dhmm(test_data_07,[0.5 0.5],cell_real_trans_and_emiss_video{i,2}{1,2},cell_real_trans_and_emiss_video{i,2}{1,3});
        
    end

a1_s = loglik_test_data(1,:,1) > loglik_test_data(1,:,2);
b1_s = loglik_test_data(1,:,4) > loglik_test_data(1,:,3);
c1_s = sum(a1_s)+sum(b1_s);
efficiency_a_sineorwords = c1_s/8 *100; 

f1 = fopen('Results.txt','a');
fprintf(f1,'%d',efficiency_a_sineorwords);
fclose(f1);
end
