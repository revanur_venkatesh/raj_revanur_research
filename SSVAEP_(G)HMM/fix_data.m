fname = 'HMM_cca.mat';
load(fname);

fieldnames = {'sine07', 'sine37', 'words07', 'words37'};

for field = fieldnames
    v = hmmData.(field{1});
    for index = 1:size(v,2)
       a = v(1,index);
       a{1} = 3 - a{1};
       v(1,index) = a;
    end
    hmmData.(field{1}) = v;
end

save(fname, 'hmmData')