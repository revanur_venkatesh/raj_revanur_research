function [] = GHMM_Video_web(wors)
clc;
%% CORE PART

attach = {'KPMstats','netlab3.3','HMM','GraphViz','KMPtools'};
initial_path = 'C:\Users\Revanur Venkatesh\Documents\raj_revanur_research\SSVAEP_(G)HMM\bnt-master\bnt-master';

addpath 'C:\Users\Revanur Venkatesh\Documents\raj_revanur_research\SSVAEP_(G)HMM\bnt-master\bnt-master\KPMstats';
addpath 'C:\Users\Revanur Venkatesh\Documents\raj_revanur_research\SSVAEP_(G)HMM\bnt-master\bnt-master\netlab3.3';
addpath 'C:\Users\Revanur Venkatesh\Documents\raj_revanur_research\SSVAEP_(G)HMM\bnt-master\bnt-master\HMM';
addpath 'C:\Users\Revanur Venkatesh\Documents\raj_revanur_research\SSVAEP_(G)HMM\bnt-master\bnt-master\GraphViz';
addpath 'C:\Users\Revanur Venkatesh\Documents\raj_revanur_research\SSVAEP_(G)HMM\bnt-master\bnt-master\KPMtools';


%% VandA Init
load('webdatahmmgmm.mat');
load('GHMM_sft.mat');
M = 2;
Q = 2;

%% FOR VIDEO

v_types = {'sine','words'};
v_electrodes = {'O1','Oz','O2'};
v_freq = {'07','10'};
array_trans = [];
array_emmis = [];
store_index_test= {};
count = 0;
jump_index = 0;
M = 2;
Q = 2;
t = 1;
    for i=1:size(v_electrodes,2)
        for j=1:size(v_freq,2)
            
            access_data_string = strcat(v_types(wors),v_electrodes(i),v_freq(j));
            access_size_string = strcat(v_types(wors),v_electrodes(i),v_freq(j),'index');
            %         for k=1:hmmData.(access_size_string{1,1})-1
            %
            %             array_temp_hidden = hmmData.(access_data_string{1,1}){1,k};
            %             array_temp_visible = ones(size(array_temp_hidden));
            %             [trans emiss] = hmmestimate(array_temp_visible,array_temp_hidden);
            %             array_trans = [array_trans trans];
            %             array_emmis = [array_emmis emiss];
            %
            %         end
            
            O = size(hmmData.(access_data_string{1,1}) ,1);
            T = size(hmmData.(access_data_string{1,1}),2);
            nex = size(hmmData.(access_data_string{1,1}),3);
            
            [train, test] = crossvalind('HoldOut', nex, 0.5);
            index_train_data = (train)'.*(1:nex);
            index_test_data = (test)'.*(1:nex);
            
            index_train_data(index_train_data==0)=[]
            index_test_data(index_test_data==0)=[];
            
            training_data = hmmData.(access_data_string{1,1})(:,:,index_train_data)
            testing_data = hmmData.(access_data_string{1,1})(:,:,index_test_data);% cpy for test
            
            count = count + 1;
            index_test_glob{count} = index_test_data;
            
            prior0 = [0.5 0.5];
            transmat0 = [1 0;0 1];
            
            cov_type = 'full';
            [mu0, Sigma0] = mixgauss_init(Q*M, reshape(training_data, [O T*(size(training_data,3))]), cov_type);
            mu0 = reshape(mu0, [O Q M]);
            Sigma0 = reshape(Sigma0, [O O Q M]);
            mixmat0 = mk_stochastic(rand(Q,M));
            
            [ll_trace, prior, transmat, mu, sigma, mixmat] = mhmm_em(training_data, prior0, transmat0, mu0, Sigma0, mixmat0, 'max_iter', 100);
            loglik_train{t,i,j} = mhmm_logprob(training_data, prior, transmat, mu, sigma, mixmat);
            cell_real_trans_and_emiss_video {t,i,j} = [access_data_string ll_trace, prior, transmat, mu, sigma, mixmat];
            
            left_right = 0;
            
        end
    end

i=1;
    for j=1:size(v_electrodes,2)
        
        access_data_string_07 = strcat(v_types(wors),v_electrodes(j),'07');
        %access_size_string = strcat(v_types(i),v_electrodes(j),v_freq(j),'index');
        access_data_string_10 = strcat(v_types(wors),v_electrodes(j),'10');
        
        % FOR TESTING DATA 07 -------------------------------------------------
        
        %temp_cell = hmmData.(access_data_string_07{1,1});
        jump_index = jump_index + 1;
        array_test_obtained = index_test_glob(jump_index);
        %test_data_07 = temp_cell(array_test_obtained{1,1});
        testing_data_07 = hmmData.(access_data_string_07{1,1})(:,:,array_test_obtained{1,1});
        
        loglik_test_data(i,j,1) = mhmm_logprob(testing_data_07, cell_real_trans_and_emiss_video{i,j,1}{1,3}, cell_real_trans_and_emiss_video{i,j,1}{1,4}, cell_real_trans_and_emiss_video{i,j,1}{1,5}, cell_real_trans_and_emiss_video{i,j,1}{1,6}, cell_real_trans_and_emiss_video{i,j,1}{1,7});
        loglik_test_data(i,j,2) = mhmm_logprob(testing_data_07, cell_real_trans_and_emiss_video{i,j,2}{1,3}, cell_real_trans_and_emiss_video{i,j,2}{1,4}, cell_real_trans_and_emiss_video{i,j,2}{1,5}, cell_real_trans_and_emiss_video{i,j,2}{1,6}, cell_real_trans_and_emiss_video{i,j,2}{1,7});
        %     u_test(i,1) = log_lik_dhmm(test_data_07,[0.5 0.5],cell_real_trans_and_emiss_video{i,1}{1,2},cell_real_trans_and_emiss_video{i,1}{1,3});
        %     u_test(i,2) = log_lik_dhmm(test_data_07,[0.5 0.5],cell_real_trans_and_emiss_video{i,2}{1,2},cell_real_trans_and_emiss_video{i,2}{1,3});
        
        % FOR TESTING DATA 10 -------------------------------------------------
        %[train, test] = crossvalind('HoldOut', size(hmmData.(access_data_string_10{1,1}),2), 0.1);
        %index_test_data = (test)'.*(1:size(hmmData.(access_data_string_10{1,1}),2));
        
        %temp_cell = hmmData.(access_data_string_10{1,1});
        jump_index = jump_index + 1;
        array_test_obtained = index_test_glob(jump_index);
        %test_data_07 = temp_cell(array_test_obtained{1,1});
        testing_data_10 = hmmData.(access_data_string_10{1,1})(:,:,array_test_obtained{1,1});
        
        loglik_test_data(i,j,3) = mhmm_logprob(testing_data_10, cell_real_trans_and_emiss_video{i,j,1}{1,3}, cell_real_trans_and_emiss_video{i,j,1}{1,4}, cell_real_trans_and_emiss_video{i,j,1}{1,5}, cell_real_trans_and_emiss_video{i,1}{1,6}, cell_real_trans_and_emiss_video{i,j,1}{1,7});
        loglik_test_data(i,j,4) = mhmm_logprob(testing_data_10, cell_real_trans_and_emiss_video{i,j,2}{1,3}, cell_real_trans_and_emiss_video{i,j,2}{1,4}, cell_real_trans_and_emiss_video{i,j,2}{1,5}, cell_real_trans_and_emiss_video{i,j,2}{1,6}, cell_real_trans_and_emiss_video{i,j,2}{1,7});
        %     u_test(i,1) = log_lik_dhmm(test_data_07,[0.5 0.5],cell_real_trans_and_emiss_video{i,1}{1,2},cell_real_trans_and_emiss_video{i,1}{1,3});
        %     u_test(i,2) = log_lik_dhmm(test_data_07,[0.5 0.5],cell_real_trans_and_emiss_video{i,2}{1,2},cell_real_trans_and_emiss_video{i,2}{1,3});
        
        
    end

a1_s = loglik_test_data(1,:,1) > loglik_test_data(1,:,2);
b1_s = loglik_test_data(1,:,4) > loglik_test_data(1,:,3);
c1_s = sum(a1_s)+sum(b1_s);
efficiency_v_sineorwords = c1_s/6 *100; 

f1 = fopen('Results.txt','w');
fprintf(f1,'%d\n',efficiency_v_sineorwords);
fclose(f1);
end
%%
