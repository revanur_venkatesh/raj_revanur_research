clc;
clear all;
tic;
load('Loglikelihoods_HMM_CCA.mat');

[train, test] = crossvalind('HoldOut', size(log_likelihoods,2), 0.1);
index_train_data = (train)'.*(1:size(log_likelihoods,2));
index_test_data = (test)'.*(1:size(log_likelihoods,2));
index_train_data(index_train_data==0)=[];
index_test_data(index_test_data==0)=[];
 
labels = log_likelihoods(3,index_test_data);

svm_trained_model = svmtrain(log_likelihoods([1 2],index_train_data)',log_likelihoods(3,index_train_data)')
svm_test_output_labels = svmclassify(svm_trained_model,log_likelihoods([1 2],index_test_data)')
a = (labels' == svm_test_output_labels)
sum(a)

md1 = fitcknn(log_likelihoods([1 2],index_train_data)',log_likelihoods(3,index_train_data)','NumNeighbors',5)
labels_got = predict(md1,log_likelihoods([1 2],index_test_data)')
b = (labels'==labels_got)
sum(b)
pause;

% hmmData_for7hz = cell2mat(hmmData.sine07')';
% labels_for7hz = ones(1, size(hmmData_for7hz, 2)) * 1;
% hmmData_for7hz = [hmmData_for7hz; labels_for7hz];
% 
% hmmData_for10hz = cell2mat(hmmData.sine10')';
% labels_for10hz = ones(1, size(hmmData_for10hz, 2)) * 2;
% hmmData_for10hz = [hmmData_for10hz; labels_for10hz];
% 
% hmmData_video = [hmmData_for7hz hmmData_for10hz];
% 
% index_vector = randperm(size(hmmData_video, 2));
% hmmData_video_randomised = zeros(size(hmmData_video));
% for k = 1:size(hmmData_video, 2)
%     hmmData_video_randomised(:, k) = hmmData_video(:, index_vector(k));
% end
% 
% % [train, test] = crossvalind('HoldOut', size(hmmData_video_randomised, 2), 0.1);
% log_likelihoods = zeros(3, size(hmmData_video_randomised, 2));
% for index = 1:floor(size(hmmData_video_randomised, 2)/10):size(hmmData_video_randomised, 2)
%     
%     train = ones(size(hmmData_video_randomised, 2), 1);
%     if index + floor(size(hmmData_video_randomised, 2)/10) - 1 <= size(hmmData_video_randomised, 2)
%         indices = index : index + floor(size(hmmData_video_randomised, 2)/10) - 1;
%         train(indices) = 0;
%     else
%         indices = index : size(hmmData_video_randomised, 2);
%         train(indices) = 0;
%     end
%     test = 1 - train;
%     
% index_train_data = (train)'.*(1:size(hmmData_video_randomised, 2));
% index_test_data = (test)'.*(1:size(hmmData_video_randomised, 2));
% index_train_data(index_train_data==0)=[];
% index_test_data(index_test_data==0)=[];
% 
% train_data = hmmData_video_randomised(:, index_train_data);
% test_data = hmmData_video_randomised(1:end-1, index_test_data);
% test_labels = hmmData_video_randomised(end, index_test_data);
% 
% [prior_learnt_1, transmat_learnt_1, emmat_learnt_1] = training_HMM(train_data(1:end-1, train_data(end,:)==1), 1);
% [prior_learnt_2, transmat_learnt_2, emmat_learnt_2] = training_HMM(train_data(1:end-1, train_data(end,:)==2), 2);
% 
% % toc;
% 
% [acc, log_lik] = testing_HMM(prior_learnt_1, transmat_learnt_1, emmat_learnt_1, prior_learnt_2, transmat_learnt_2, emmat_learnt_2, test_data, test_labels);
% log_likelihoods(1:end-1, logical(test)) = log_lik;
% log_likelihoods(end,logical(test)) = test_labels;
% 
% % pause
% % toc;
% end
% figure;
% plot(log_likelihoods(1, log_likelihoods(end,:)==1), log_likelihoods(2, log_likelihoods(end,:)==1), 'o');
% hold on;
% plot(log_likelihoods(1, log_likelihoods(end,:)==2), log_likelihoods(2, log_likelihoods(end,:)==2), 'x');