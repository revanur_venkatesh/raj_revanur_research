clc;
%load('webdatahmm.mat');
string_for_loading = ['HMM_cwt.mat','HMM_sft.mat','webdatahmm.mat']
load('HMM_cca.mat');
tic;
%% For Audio

a_type = {'sine','words'};
a_electrodes = {'T3','Cz','T4','Oz'};
a_freq = {'37','43'};
array_trans = [];
array_emmis = [];
store_index_test= {};
count = 0;
jump_index = 0;

for algo =1:size(string_for_loading,2)
end
t = 1;
    for i=1:size(a_electrodes,2)
        for j=1:size(a_freq,2)
            
            access_data_string = strcat(a_type(wors),a_electrodes(i),a_freq(j));
            access_size_string = strcat(a_type(wors),a_electrodes(i),a_freq(j),'index');
            %             for k=1:hmmData.(access_size_string{1,1})-1
            %
            %                 array_temp_hidden = hmmData.(access_data_string{1,1}){1,k};
            %                 array_temp_visible = ones(size(array_temp_hidden));
            %                 [trans emiss] = hmmestimate(array_temp_visible,array_temp_hidden);
            %                 array_trans =[array_trans trans];
            %                 array_emmis = [array_emmis emiss];
            % 
            %             end
            
            
            [train, test] = crossvalind('HoldOut', size(hmmData.(access_data_string{1,1}),2), 0.1);
            index_train_data = (train)'.*(1:size(hmmData.(access_data_string{1,1}),2));
            index_test_data = (test)'.*(1:size(hmmData.(access_data_string{1,1}),2));
            index_train_data(index_train_data==0)=[];
            index_test_data(index_test_data==0)=[];
            count = count + 1;
            index_test_glob{count} = index_test_data;
            
            temp_cell = hmmData.(access_data_string{1,1});
            temp_cell_arrays_train_audio = temp_cell(index_train_data);
            temp_cell_arrays_test_audio = temp_cell(index_test_data);
            
            [real_trans real_emiss] = hmmtrain(temp_cell_arrays_train_audio,[0.7 0.3;0.3 0.7],[0.95 0.05;0.05 0.95]);
            cell_real_trans_and_emiss_audio {t,i,j} = [access_data_string real_trans real_emiss];
            u_train_audio(t,i,j) = log_lik_dhmm(temp_cell_arrays_train_audio,[0.5 0.5],real_trans,real_emiss);
            
            
            %[real_trans real_emiss] = hmmtrain(hmmData.(access_data_string{1,1}),[0.7 0.3;0.3 0.7],[0.95 0.05;0.05 0.95]);
            %cell_real_trans_and_emiss_audio {t,i,j} = [access_data_string real_trans real_emiss];
            %u1(i,j) = log_lik_dhmm(hmmData.(access_data_string{1,1}),[1 0],real_trans,real_emiss);
            
        end
    end


i =1;
    for j=1:size(a_electrodes,2)
        
        access_data_string_37 = strcat(a_type(wors),a_electrodes(j),'37');
        %        access_size_string = strcat(a_electrodes(i),a_freq(j),'index');
        access_data_string_43 = strcat(a_type(wors),a_electrodes(j),'43');
        
        % FOR TESTING DATA 37 ------------------------------------------------
        [train, test] = crossvalind('HoldOut', size(hmmData.(access_data_string_37{1,1}),2), 0.1);
        index_test_data = (test)'.*(1:size(hmmData.(access_data_string_37{1,1}),2));
        
        index_test_data(index_test_data==0)=[];
        temp_cell = hmmData.(access_data_string_37{1,1});
        jump_index = jump_index + 1;
        array_test_obtained = index_test_glob(jump_index);
        test_data_37 = temp_cell(array_test_obtained{1,1});
        
        u_test_audio(i,j,1) = log_lik_dhmm(test_data_37,[0.5 0.5],cell_real_trans_and_emiss_audio{i,j,1}{1,2},cell_real_trans_and_emiss_audio{i,j,1}{1,3});
        u_test_audio(i,j,2) = log_lik_dhmm(test_data_37,[0.5 0.5],cell_real_trans_and_emiss_audio{i,j,2}{1,2},cell_real_trans_and_emiss_audio{i,j,2}{1,3});
        
        % FOR TESTING DATA 43 -------------------------------------------------
        [train, test] = crossvalind('HoldOut', size(hmmData.(access_data_string_43{1,1}),2), 0.1);
        index_test_data = (test)'.*(1:size(hmmData.(access_data_string_43{1,1}),2));
        
        index_test_data(index_test_data==0)=[];
        temp_cell = hmmData.(access_data_string_43{1,1});
        jump_index = jump_index + 1;
        array_test_obtained = index_test_glob(jump_index);
        test_data_43 = temp_cell(array_test_obtained{1,1});
        u_test_audio(i,j,3) = log_lik_dhmm(test_data_43,[0.5 0.5],cell_real_trans_and_emiss_audio{i,j,1}{1,2},cell_real_trans_and_emiss_audio{i,j,1}{1,3});
        u_test_audio(i,j,4) = log_lik_dhmm(test_data_43,[0.5 0.5],cell_real_trans_and_emiss_audio{i,j,2}{1,2},cell_real_trans_and_emiss_audio{i,j,2}{1,3});
        
    end

a1_s = u_test_audio(1,:,1) > u_test_audio(1,:,2);
b1_s = u_test_audio(1,:,4) > u_test_audio(1,:,3);
c1_s = sum(a1_s)+sum(b1_s);
efficiency_a_sineorwords = c1_s/8 *100; 
f1 = fopen('Results.txt','a');
fprintf(f1,'%d',efficiency_a_sineorwords);
fclose(f1);
clear all;
toc;
%%