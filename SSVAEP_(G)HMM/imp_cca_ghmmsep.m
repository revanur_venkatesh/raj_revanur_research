load('SubjectData.mat');
eL = 2;
Fs = 256;
cuts = {5:7, 1:4}; % Video and Audio channels
chNames = {'T3','Cz','T4','Oz','O1','Oz','O2'};
cha = [6 8 10 15 14 15 16];

%% 
hmmData = struct();
for cat = {'sine','words'}
    
        for f = {'07','10'}
            hmmData.(strcat(cat{1},f{1},'index')) = 0;
        end
    
end
for cat = {'sine','words'}
    
        for f = {'37','43'}
            hmmData.(strcat(cat{1},f{1},'index')) = 0;
        end
    
end

%% 
for n = 1:length(subjData)
    toIndexData = subjData(n).toIndex;
    cat = subjData(n).SorW;
    for j = 1:size(toIndexData,2)
        
        if (toIndexData{4,j} == 10)
            
                hmmData.(strcat(cat,'10','index')) = hmmData.(strcat(cat,'10','index')) + 1;
            
        else
            
                hmmData.(strcat(cat,'07','index')) = hmmData.(strcat(cat,'07','index')) + 1;
            
        end
        
        if (toIndexData{5,j} == 37)
            
                hmmData.(strcat(cat,'37','index')) = hmmData.(strcat(cat,'37','index')) + 1;
            
        else
            
                hmmData.(strcat(cat,'43','index')) = hmmData.(strcat(cat,'43','index')) + 1;
            
        end
    end
end

%% 
for n = 1:length(subjData)
    disp(n);
    toIndexData = subjData(n).toIndex;
    cat = subjData(n).SorW;
    for j = 1:size(toIndexData,2)
        
        if (toIndexData{4,j} == 10)
            
                hmmData.(strcat(cat,'10')) = cell(1,hmmData.(strcat(cat,'10','index')));
                hmmData.(strcat(cat,'10','index')) = 1;
            
        else
            
                hmmData.(strcat(cat,'07')) = cell(1,hmmData.(strcat(cat,'07','index')));
                hmmData.(strcat(cat,'07','index')) = 1;
            
        end
        
        if (toIndexData{5,j} == 37)
            
                hmmData.(strcat(cat,'37')) = cell(1,hmmData.(strcat(cat,'37','index')));
                hmmData.(strcat(cat,'37','index')) = 1;
            
        else
            
                hmmData.(strcat(cat,'43')) = cell(1,hmmData.(strcat(cat,'43','index')));
                hmmData.(strcat(cat,'43','index')) = 1;
            
        end
    end
end

%% 

time = 0:1/Fs:2;
time = time(1:end-1);
cFre = [7.5 10 37 43];
sinecos = cell(1,length(cFre));
for i=1:2
    sinecos{i} = [
        sin(2*pi*cFre(i)*time);
        cos(2*pi*cFre(i)*time);
        sin(4*pi*cFre(i)*time);
        cos(4*pi*cFre(i)*time);
        sin(6*pi*cFre(i)*time);
        cos(6*pi*cFre(i)*time)];
%     sinecos{i} = sinecos{i} + rand(size(sinecos{i}));
end
for i=3:4
    sinecos{i} = [
        sin(2*pi*cFre(i)*time);
        cos(2*pi*cFre(i)*time);
        sin(4*pi*cFre(i)*time);
        cos(4*pi*cFre(i)*time)];
%     sinecos{i} = sinecos{i} + rand(size(sinecos{i}));
end

for n = 1:length(subjData)
    disp(n);
    HDR = sopen(strcat('C:\Users\USER\Desktop\Everything related to Major\MAJOR PROJECT\DATA\ProperData\', ... 
        subjData(n).FName,'.edf'), 'r');
    [S,HDR] = sread(HDR);
    HDR = sclose(HDR);
    
    toIndexData = subjData(n).toIndex;
    fir = cell2mat(toIndexData(1,:));
    extra = cell2mat(toIndexData(2,:));
    cat = subjData(n).SorW;
    for j = 1:size(toIndexData,2)
        
        tosegdat = S(fir(j)*Fs+1+extra(j):(fir(j)+24)*Fs+extra(j),cha(cuts{1}));
        
        for i = 1:size(tosegdat,2)
            seg = buffer(tosegdat(:,i),Fs*eL,ceil(50/100*Fs*eL));
            dat(:,:,i) = seg(:,2:end);
        end
        
        for i = 1:size(dat,2)
            tocomp = squeeze(dat(:,i,:));
            [~,~,coff1] = canoncorr(tocomp,sinecos{1}');
            [~,~,coff2] = canoncorr(tocomp,sinecos{2}');
            coeffs(:,i) = [sum(coff1); sum(coff2)];
        end
        
        if (toIndexData{4,j} == 7.5)
            c = hmmData.(strcat(cat,'07'));
            c{hmmData.(strcat(cat,'07','index'))} = coeffs;
            hmmData.(strcat(cat,'07','index')) = hmmData.(strcat(cat,'07','index')) + 1;
            hmmData.(strcat(cat,'07')) = c;
        else
            c = hmmData.(strcat(cat,'10'));
            c{hmmData.(strcat(cat,'10','index'))} = coeffs([2 1],:);
            hmmData.(strcat(cat,'10','index')) = hmmData.(strcat(cat,'10','index')) + 1;
            hmmData.(strcat(cat,'10')) = c;
        end
        
        tosegdat = S(fir(j)*Fs+1+extra(j):(fir(j)+24)*Fs+extra(j),cha(cuts{2}));
        
        for i = 1:size(tosegdat,2)
            seg = buffer(tosegdat(:,i),Fs*eL,ceil(50/100*Fs*eL));
            dat(:,:,i) = seg(:,2:end);
        end
        
        for i = 1:size(dat,2)
            tocomp = squeeze(dat(:,i,:));
            [~,~,coff1] = canoncorr(tocomp,sinecos{3}');
            [~,~,coff2] = canoncorr(tocomp,sinecos{4}');
            coeffs(:,i) = [sum(coff1); sum(coff2)];
        end
        
        if (toIndexData{5,j} == 37)
            c = hmmData.(strcat(cat,'37'));
            c{hmmData.(strcat(cat,'37','index'))} = coeffs;
            hmmData.(strcat(cat,'37','index')) = hmmData.(strcat(cat,'37','index')) + 1;
            hmmData.(strcat(cat,'37')) = c;
        else
            c = hmmData.(strcat(cat,'43'));
            c{hmmData.(strcat(cat,'43','index'))} = coeffs([2 1],:);
            hmmData.(strcat(cat,'43','index')) = hmmData.(strcat(cat,'43','index')) + 1;
            hmmData.(strcat(cat,'43')) = c;
        end

    end
end

%%
for cat = {'sine','words'}
    
        for f = {'07','10'}
            index = hmmData.(strcat(cat{1},f{1},'index'))-1;
            finalData = zeros(2,23,index);
            for i=1:index
                cellData = hmmData.(strcat(cat{1},f{1}));
                finalData(:,:,i) = (cellData{i});
                
            end
            hmmData.(strcat(cat{1},f{1})) = finalData;
        end
    
end
for cat = {'sine','words'}
    
        for f = {'37','43'}
            index = hmmData.(strcat(cat{1},f{1},'index'))-1;
            finalData = zeros(2,23,index);
            for i=1:index
                cellData = hmmData.(strcat(cat{1},f{1}));
                finalData(:,:,i) = (cellData{i});
                
            end
            hmmData.(strcat(cat{1},f{1})) = finalData;
        end
    
end
