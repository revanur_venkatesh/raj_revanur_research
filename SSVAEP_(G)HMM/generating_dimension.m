%load('Loglikelihoods_HMM_CCA.mat')
loglikelihoods_quad_dimension = [ones(1,size(log_likelihoods,2)); log_likelihoods([1],:) ;log_likelihoods([2],:) ;(log_likelihoods(1,:).^2) ;(log_likelihoods(2,:).^2) ;(log_likelihoods(1,:).* log_likelihoods(2,:)) ;log_likelihoods(3,:)];

Y = (log_likelihoods(3,:)==1) ? [1 0] : [0 1];
for i=1:size(log_likelihoods(3,:),2)
if log_likelihoods(3,i)==1
    Y = [Y;1 0];
else
    Y = [Y;0 1];
end
end